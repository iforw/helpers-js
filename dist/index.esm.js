const downscaleImage = (dataUrl, newWidth, imageType, imageArguments) => {
  let image, oldWidth, oldHeight, newHeight, canvas, ctx, newDataUrl; // Provide default values

  imageType = imageType || "image/jpeg";
  imageArguments = imageArguments || 0.7; // Create a temporary image so that we can compute the height of the downscaled image.

  image = new Image();
  image.src = dataUrl;
  oldWidth = image.width;
  oldHeight = image.height;
  newHeight = Math.floor(oldHeight / oldWidth * newWidth); // Create a temporary canvas to draw the downscaled image on.

  canvas = document.createElement("canvas");
  canvas.width = newWidth;
  canvas.height = newHeight; // Draw the downscaled image on the canvas and return the new data URL.

  ctx = canvas.getContext("2d");
  ctx.drawImage(image, 0, 0, newWidth, newHeight);
  newDataUrl = canvas.toDataURL(imageType, imageArguments);
  return newDataUrl;
};

const parseDuration = seconds => {
  const pad = string => ("0" + string).slice(-2);

  const date = new Date(seconds * 1000);
  const hh = date.getUTCHours();
  const mm = date.getUTCMinutes();
  const ss = pad(date.getUTCSeconds());

  if (hh) {
    return `${hh}:${pad(mm)}:${ss}`;
  }

  return `${mm}:${ss}`;
};

const parseError = (valueKey, error) => {
  if (error && error.response && error.response.data && error.response.data.errors && error.response.data.errors[valueKey]) {
    return error.response.data.errors[valueKey][0];
  }

  if (error && error.errors && error.errors[valueKey]) {
    return error.errors[valueKey][0];
  }

  return null;
};

const parseErrorMessage = (error, defaultMessage) => {
  // console.error("error", error);
  if (error && error.response) {
    // console.log("error not defined, setting error to error.response");
    error = error.response;
  }

  if (error && error.data) {
    // console.log("error not defined, setting error to error.data");
    error = error.data;
  }

  if (error && error.errors) {
    // console.log("error.errors exists, checking if there's also message");
    let message = "";

    if (error.message && typeof error.message === "string") {
      message += error.message + " ";
    }

    if (typeof error.errors === "object") {
      // console.log("error.errors is an object, implode by key");
      Object.keys(error.errors).map(key => {
        if (typeof error.errors[key][0] === "string") {
          message += error.errors[key][0] + " ";
        }
      });
    }

    return message.trim();
  }

  if (error && error.message && typeof error.message === "string") {
    // console.log("error.message exists, return message");
    return error.message.trim();
  }

  if (error && error.response && error.response.data && error.response.data.error && typeof error.response.data.error === "string") {
    let message = error.response.data.error;

    if (error.response.statusText) {
      message = `${error.response.statusText}: ${message}`;
    }

    return message;
  }

  if (error && error.response && error.response.data && error.response.data.message && typeof error.response.data.message === "string") {
    return error.response.data.message;
  }

  return defaultMessage;
};

const parseStatusCode = error => {
  let code = 500; // console.error("error", error);

  if (error && error.response && error.response.status) {
    // console.log(error.response, "setting error code from status");
    code = error.response.status;
  }

  return code;
};

const parseDate = (year, month, day, hour, minute) => {
  if (!year || !month | !day || !hour || !minute) {
    return null;
  }

  if (month < 10 && month.length < 2) {
    month = `0${month}`;
  }

  if (day < 10 && day.length < 2) {
    day = `0${day}`;
  }

  if (hour < 10 && hour.length < 2) {
    hour = `0${hour}`;
  }

  if (minute < 10 && minute.length < 2) {
    minute = `0${minute}`;
  }

  return `${year}-${month}-${day}T${hour}:${minute}:00.000000Z`;
};

const shuffleArray = array => {
  let currentIndex = array.length;
  let temporaryValue, randomIndex;

  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};

const reRegExpChar = /[\\^$.*+?()[\]{}|]/g;
const reHasRegExpChar = RegExp(reRegExpChar.source);

function escapeRegExp(string) {
  return string && reHasRegExpChar.test(string) ? string.replace(reRegExpChar, "\\$&") : string;
}

const trimChar = (origString, charToTrim) => {
  charToTrim = escapeRegExp(charToTrim);
  let regEx = new RegExp("^[" + charToTrim + "]+|[" + charToTrim + "]+$", "g");
  return origString.replace(regEx, "");
};

const pluralise = string => {
  if (typeof string !== "string") return null;

  if (string.substr(-1) === "s") {
    return string + "es";
  }

  if (string.substr(-1) === "y") {
    return string.slice(0, string.length - 1) + "ies";
  }

  return string + "s";
};

const reorderArray = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

export { downscaleImage, parseDate, parseDuration, parseError, parseErrorMessage, parseStatusCode, pluralise, reorderArray, shuffleArray, trimChar };
//# sourceMappingURL=index.esm.js.map
