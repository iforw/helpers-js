const parseDate = (year, month, day, hour, minute) => {
  if (!year || !month | !day || !hour || !minute) {
    return null;
  }

  if (month < 10 && month.length < 2) {
    month = `0${month}`;
  }

  if (day < 10 && day.length < 2) {
    day = `0${day}`;
  }

  if (hour < 10 && hour.length < 2) {
    hour = `0${hour}`;
  }

  if (minute < 10 && minute.length < 2) {
    minute = `0${minute}`;
  }

  return `${year}-${month}-${day}T${hour}:${minute}:00.000000Z`;
};

export default parseDate;
