const parseStatusCode = error => {
  let code = 500;

  // console.error("error", error);

  if (error && error.response && error.response.status) {
    // console.log(error.response, "setting error code from status");
    code = error.response.status;
  }

  return code;
};

export default parseStatusCode;
