const parseError = (valueKey, error) => {
  if (
    error &&
    error.response &&
    error.response.data &&
    error.response.data.errors &&
    error.response.data.errors[valueKey]
  ) {
    return error.response.data.errors[valueKey][0];
  }

  if (error && error.errors && error.errors[valueKey]) {
    return error.errors[valueKey][0];
  }

  return null;
};

export default parseError;
