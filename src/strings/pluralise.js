const pluralise = (string) => {
  if (typeof string !== "string") return null;

  if (string.substr(-1) === "s") {
    return string + "es";
  }

  if (string.substr(-1) === "y") {
    return string.slice(0, string.length - 1) + "ies";
  }

  return string + "s";
};

export default pluralise;
